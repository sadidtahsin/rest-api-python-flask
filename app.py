from flask import Flask, jsonify,abort,request,make_response,g,Session
from flask_httpauth import HTTPTokenAuth,HTTPBasicAuth
import hashlib



salt = "trs".encode()
app = Flask(__name__)
session=Session()
session['username']='td'
auth= HTTPTokenAuth('Bearer')

basicAuth= HTTPBasicAuth()

app.config['SECRET_KEY'] = 'a@b@345567'

tokens = {
    "secret-token-1": "john",
    "secret-token-2": "susan"
}

@auth.verify_token
def verify_token(token):
    print(token+"   "+app.secret_key)
    if token==app.secret_key:
        
        return True
    return False

@basicAuth.get_password
def get_password(username):
    if username == 'tamim.tm':
        password= 'mypassword'.encode()
        hash_pass= hashlib.sha256(password+salt)
        return hash_pass.hexdigest()
    
    return None

@basicAuth.hash_password
def hash_pw(username, password):
    password= password.encode()
    hash_pass= hashlib.sha256(password+salt)
    return hash_pass.hexdigest()

@basicAuth.error_handler
def unauthorized():
    
    return make_response(jsonify({'error': 'Unauthorized access'}), 403)

    

url='/test/api/v1'

@app.route('/')
def index():
    return 'hello world'
tasks= [
    {
        'id' : 1,
        'name': 'Sadid Tahsin',
        'age': 25

    },
    {
        'id' : 2,
        'name': 'Mowmita Tahsin',
        'age': 20
    }
]

@app.route(url+'/tasks',methods=['GET'])
# @auth.login_required
def get_tasks():
    if 'admin' in session['username']:
        print('active')
    else:
        print('inactive')    
    return jsonify({'mytasks': tasks})

@app.route(url+'/tasks/<int:task_id>',methods=['GET'])
@basicAuth.login_required
def get_task(task_id):
    task = [ task for task in tasks if task['id'] == task_id ]

    if len(task)==0:
        abort(404)
    return jsonify({"task":task})




@app.route(url+'/tasks', methods=['POST'])
def post_task():
    if not request.json:
        abort(400)
    
    task={
        'id': tasks[-1]['id']+1,
        'name' : request.json['name'],
        'age' : request.json['age']

    }

    tasks.append(task)
    return jsonify({'task':task}), 201


@app.route(url+"/tasks/<int:task_id>",methods=['PUT'])
def update_task(task_id):
    if not request.json:
        abort(400)

    index=0
    for i in range(len(tasks)):
        if(tasks[i]['id']==task_id):
            index=i
            tasks[i]['name'] =request.json.get('name',tasks[i]['name'])
            tasks[i]['age'] = request.json.get('age',tasks[i]['age'])

    return jsonify({'task': tasks[index]})

@app.route(url+"/tasks/<int:task_id>",methods=['DELETE'])
def delete_tasks(task_id):
    task = [task for task in tasks if task['id']== task_id ]

    if len(task)==0:
        abort(404)
    tasks.remove(task[0])
    return jsonify({'tasks':task})
    


if __name__ == '__main__':
    app.run(threaded=True,host='0.0.0.0', debug=True)